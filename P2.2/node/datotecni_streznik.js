var http = require("http"), fs = require("fs");

var metode = Object.create(null);

http.createServer(function(zahteva, odgovor) {
  function odgovori(kodaStatus, vsebina, tip) {
    if (!tip) tip = "text/plain";
    odgovor.writeHead(kodaStatus, {"Content-Type": tip});
    if (vsebina && vsebina.pipe)
      vsebina.pipe(odgovor);
    else
      odgovor.end(vsebina);
  }
  if (zahteva.method in metode)
    metode[zahteva.method](urlVPot(zahteva.url), odgovori, zahteva);
  else
    odgovori(405, "Metoda " + zahteva.method + " ni dovoljena.");
}).listen(process.env.PORT || 8000, function() {
  console.log("Strežnik pognan.");
});

function urlVPot(url) {
  var pot = require("url").parse(url).pathname;
  return "." + decodeURIComponent(pot);
}

metode.GET = function(pot, odgovori) {
  fs.stat(pot, function(napaka, podatki) {
    if (napaka && napaka.code == "ENOENT")
      odgovori(404, "Datoteke ni mogoče najti");
    else if (napaka)
      odgovori(500, napaka.toString());
    else if (podatki.isDirectory())
      fs.readdir(pot, function(napaka, datoteke) {
        if (napaka)
          odgovori(500, napaka.toString());
        else
          odgovori(200, datoteke.join("\n"));
      });
    else
      odgovori(200, fs.createReadStream(pot), require("mime").getType(pot));
  });
};

metode.DELETE = function(pot, odgovori) {
  fs.stat(pot, function(napaka, podatki) {
    if (napaka && napaka.code == "ENOENT")
      odgovori(204);
    else if (napaka)
      odgovori(500, napaka.toString());
    else if (podatki.isDirectory())
      fs.rmdir(pot, odgovoriNapakoAliNic(odgovori));
    else
      fs.unlink(pot, odgovoriNapakoAliNic(odgovori));
  });
};

function odgovoriNapakoAliNic(odgovori) {
  return function(napaka) {
    if (napaka)
      odgovori(500, napaka.toString());
    else
      odgovori(204);
  };
}

metode.PUT = function(pot, odgovori, zahteva) {
  var odhodniTokPodatkov = fs.createWriteStream(pot);
  odhodniTokPodatkov.on("error", function(napaka) {
    odgovori(500, napaka.toString());
  });
  odhodniTokPodatkov.on("finish", function() {
    odgovori(204);
  });
  zahteva.pipe(odhodniTokPodatkov);
};