var fs = require("fs");

fs.readFile("datoteka.txt", function(napaka, medpomnilnik) {
  if (napaka)
    throw napaka;
  console.log("Datoteka vsebuje", medpomnilnik.length, "bajtov.", "Prvi bajt je:", medpomnilnik[0], ".");
});