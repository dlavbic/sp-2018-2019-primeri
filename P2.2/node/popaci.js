module.exports = function(niz) {
  return niz.split("").map(function(znak) {
    return String.fromCharCode(znak.charCodeAt(0) + 5);
  }).join("");
};