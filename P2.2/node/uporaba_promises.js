var Promise = require("promise");
var fs = require("fs");

var readFile = Promise.denodeify(fs.readFile);
readFile("datoteka.txt", "utf8").then(function(vsebina) {
  console.log("Datoteka vsebuje:", vsebina);
}, function(napaka) {
  console.log("Napaka pri branju datoteke:", napaka);
});