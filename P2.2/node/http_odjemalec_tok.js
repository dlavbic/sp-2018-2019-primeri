var http = require("http");

var zahteva = http.request({
  hostname: "localhost",
  port: process.env.PORT || 8000,
  method: "POST"
}, function(odgovor) {
  odgovor.on("data", function(delcek) {
    process.stdout.write(delcek.toString());
  });
});
zahteva.end("Še malo in bomo končali.");