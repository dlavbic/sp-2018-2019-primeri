var fs = require("fs");

fs.writeFile("grafit.txt", "Spletno programiranje in action.", function(napaka) {
  if (napaka)
    console.log("Napaka pri pisanju v datoteko:", napaka);
  else
    console.log("Pisanje v datoteko je uspešno končano.");
});