# Spletno programiranje 2018/2019

Repozitorij s primeri je del [interaktivne skripte](https://teaching.lavbic.net/SP/) pri predmetu **Spletno programiranje**, ki ga v študijskem letu 2018/2019 izvaja [doc. dr. Dejan Lavbič](http://www.lavbic.net/) na [Fakulteti za računalništvo in informatiko](http://www.fri.uni-lj.si/), [Univerze v Ljubljani](http://www.uni-lj.si/).
